Multitouch sur Diskio Pi
=========================

La tablette Diskio Pi est fournie avec l'écran **Ilitek Multi-Touch-V3000** qui autorise le Multitouch sur 1O points.

Malheureusement, jusqu'à récemment, il n'était pas possible d'en profiter.

En cherchant sur Internet, j'ai fini par trouver [le logiciel Touchegg](https://github.com/JoseExposito/touchegg) : ce logiciel est développé en Open Source par José Exposito depuis 2011 : il fonctionnait sur les pads des ordinateurs portables depuis cette date mais, en 2020, le développeur a sorti une version 2 qui marche également sur les surfaces tactiles.

Installation
============

Il n'existe pas de paquets disponibles pour Raspberry Pi OS ( Rasbian); En fait, il existe bien un paquet Debian mais pour les architectures 64 bits.

Il faut donc récupérer le source du logiciel et le compiler :

1. ) Ubuntu, Debian et dérivés ( dont Raspberry Pi OS ):
- Installer les dépendances 

`$ sudo apt-get install git build-essential gdb cmake debhelper
  libudev-dev libinput-dev libpugixml-dev libcairo2-dev libx11-dev libxtst-dev libxrandr-dev libxi-dev 
  libgtk-3-dev `

- Compiler le logiciel 

`$ git clone https://github.com/JoseExposito/touchegg.git`

`$ cd touchegg`

`$ mkdir build`

`$ cd build`

`$ cmake -DCMAKE_BUILD_TYPE=Release ..`

`$ make`

`$ make install`

2. ) Slackware

- A partir de la version _current_, la seule dépendance est _pugixml_ dont on récupère le Slackbuild  [ici](https://github.com/Ponce/slackbuilds/tree/current/libraries/pugixml)

- Puis on compile le logiciel comme expliqué ci-dessus

Utilisation
===========

Touchegg fonctionne avec trois composantes :

1. Un _démon_ qui fait l'interface avec la librairie `libinput` de GNU Linux et le matériel 
2. Un _client_ qui détécte les gestes de l'utilisateur et les transmet au démon
3. Un _fichier de configuration_ qui décrit les gestes qu'il faut repérer et les actions qu'il faut leur attribuer

Démon
=====

On le lance par la commande : `touchegg --daemon`.

Il faut le lancer en premier **avant** le client; le mieux est de le faire lancer automatiquement par le système en mettant la commande précédente dans le champ _Exec_ du fichier **touchegg.desktop** qui se trouve dans `/etc/xdg/autostart`

Il va détecter le matériel et le confirmer par le message :

    Starting Touchégg in daemon mode
    Starting daemon server...
    Server started
    A list of detected compatible devices will be displayed below:
    Compatible device detected:
	Name: ILITEK Multi-Touch-V3000
	Size: 528.516mm x 300mm
	Calculating start_threshold and finish_threshold. You can tune this values in your service file
	start_threshold: 15
	finish_threshold_horizontal: 132.129
	finish_threshold_vertical: 75

Client
======

On le lance par la commande : `touchegg --client`.

Il faut le lancer **après** le démon. On peut le faire lancer par son _window manager_ (KDE, XFCE, Gnome, ..); Pour KDE, il suffit de créer un script qui lance la commande et de placer ce script dans `.kde/Autostart`.

Le client est très verbeux mais c'est très utile pour débugger la configuration :

    Starting Touchégg in client mode
    Parsing you configuration file...
    Using configuration file "/home/pi/.config/touchegg/touchegg.conf"
    Configuration parsed successfully
    Connecting to Touchégg daemon...
    Successfully connected to Touchégg daemon
    Gesture begin detected
	Gesture information:
		Fingers: 2
		Type: TAP
		Direction: UNKNOWN
	Gesture performed on app: Firefox-esr
	Action configured for this gesture
	Starting action
    Gesture end detected
    Gesture begin detected
	Gesture information:
		Fingers: 2
		Type: TAP
		Direction: UNKNOWN
	Gesture performed on app: Firefox-esr
	Action configured for this gesture
	Starting action

Configuration
=============

C'est le coeur du système qui décrit comment interpéter nos gestes. Le fichier de configuration est écrit en XML; Il se trouve :
- Dans `/usr/share/touchegg` : le fichier **touchegg.conf** situé ici est celui par défaut
- Dans `.config/touchegg/ `: vous pouvez mettre le fichier **touchegg.conf** ici pour surcharger le défaut; Si un fichier exite ici, il sera pris à la place du défaut.

Il est possible de détecter :
- les gestes à 2,3, 4 ou 5 doigts
- les swipe ( glissement), les pinch ( pincements) et les taps(frappe) dans les quatre directions : haut, bas, gauche, droit et vers l'intérieur ou vers l'extérieur
- on peut aussi animer les transitions

Pour une description détaillée et une visualisation des effets, je vous renvoie sur [le site du développeur](https://github.com/JoseExposito/touchegg).

Pour démarrer, je vous ai mis sur le site un fichier de configuration qui permet :

- Avec 3 doigts et en glissant (SWIPE) 
    - vers le haut : maximiser la fenêtre
    - vers le bas : réduire la fenêtre
    - vers la gauche : caler la fenêtre à gauche
    - vers la droite : caler la fenêtre à droite
- Avec 3 doigts et en pinçant ( PINCH) vers l'intérieur
    - fermer la fenêtre
- Frappe
    - Avec deux doigts : Bouton 3 de la souris : menu de l'application
    - Avec trois doigts : Bouton 2 de la souris: Coller ( une sélection faite avec un doigt )
- Pincement à 2 doigts
    - Vers l'intérieur : rétrécir le contenu
    - Vers l'extérieur : agrandir le contenu
- Glissement à 2 doigts 
    - vers le bas : faire descendre la page
    - vers le haut : faire monter la page

Enfin , vous pouvez spécialiser des gestes par application ; je vous ai mis un exemple qui me sert pour mes supports de cours sur Firefox en envoyant les touches gauche et droite.

A vous de définir les gestes et les actions dont vous avez besoin.
